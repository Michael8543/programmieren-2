#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Configuration.h"
#include "Console.h"
#include <QGraphicsRectItem>
#include "Aufgabe.h"
#include <QPushButton>
#include <QKeyEvent>
#include "keypress.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    KeyPress keyPress;
    keyPress.show();

    //--------------------------------------------------------------------------
    // Setzt das Hintergrundbild des MainWindow
    //--------------------------------------------------------------------------
    this->setAutoFillBackground(true); //In einigen Qt-Versionen notwendig
    QPalette qpalette;
    QPixmap pixmap = QPixmap(":/bilder/hintergrund.png");
    pixmap.scaled(CONFIGURATION::SPIELFELD_DIMENSION_X*CONFIGURATION::CONSOLE_FAKTOR_GUI,
                  CONFIGURATION::SPIELFELD_DIMENSION_Y*CONFIGURATION::CONSOLE_FAKTOR_GUI);
    qpalette.setBrush( backgroundRole(), pixmap);
    this->setPalette (qpalette); // Bild wird gekachelt, falls es keliner als das Widget ist

    //--------------------------------------------------------------------------
    // Setzt das Layout der QPushButton
    //--------------------------------------------------------------------------
    QColor col = QColor(Qt::gray);
    if(col.isValid()) {
       QString qss = QString("background-color: %1").arg(col.name());
       ui->pushButton_bewegen_hoch->setStyleSheet(qss);
       ui->pushButton_bewegen_runter->setStyleSheet(qss);
       ui->pushButton_bewegen_rechts->setStyleSheet(qss);
       ui->pushButton_bewegen_links->setStyleSheet(qss);
       ui->pushButton_drehen_rechts->setStyleSheet(qss);
       ui->pushButton_drehen_links->setStyleSheet(qss);
       ui->pushButton_flip->setStyleSheet(qss);
    }

    //--------------------------------------------------------------------------
    // Tibongo Logo als Text oder Bild
    //--------------------------------------------------------------------------
    QLabel* label_logo = ui->label_logo;
    label_logo->setText("Tibongo");
    label_logo->setStyleSheet("font: Chiller; font-size: 200; font-weight: bold; color: gray");//; color: red");
    //Optional - Bild überdeckt den Text
    label_logo->setText(QString(""));
    label_logo->setPixmap(QPixmap(":/bilder/Logo1_oH.png"));
    label_logo->setScaledContents(true);

    //--------------------------------------------------------------------------
    // Setzen der Spieler QLabel
    //--------------------------------------------------------------------------



    QLabel* label_punkte = ui->label_punkte;
    label_punkte->setText("Punkte");
    //label_punkte->setStyleSheet(lss);
    QLabel* label_eingaben = ui->label_eingaben;
    label_eingaben->setText("Eingaben");
    //label_eingaben->setStyleSheet(lss);
    QLabel* label_zeit = ui->label_zeit;
    label_zeit->setText("Restzeit");
    //label_zeit->setStyleSheet(lss);

    //-------------------------------------------------------------------------------------------------------
    //Spielfläche initialisieren - Zeichnen über blocks [CONFIGURATION::SPIELFELD_DIMENSION_X] [CONFIGURATION::SPIELFELD_DIMENSION_Y]
    //-------------------------------------------------------------------------------------------------------
    QGraphicsScene* qgs_spielfeld = new QGraphicsScene(0,0,CONFIGURATION::SPIELFELD_DIMENSION_X*CONFIGURATION::CONSOLE_FAKTOR_GUI,
                                             CONFIGURATION::SPIELFELD_DIMENSION_Y*CONFIGURATION::CONSOLE_FAKTOR_GUI, ui->graphicsView_Spielfeld);
    qgs_spielfeld->setBackgroundBrush(QBrush(Qt::black, Qt::SolidPattern));
    QGraphicsRectItem* blocks [CONFIGURATION::SPIELFELD_DIMENSION_X] [CONFIGURATION::SPIELFELD_DIMENSION_Y] ;
    for (int x = 0; x < CONFIGURATION::SPIELFELD_DIMENSION_X; x++)
        for (int y = 0; y < CONFIGURATION::SPIELFELD_DIMENSION_Y; y++)
        {
            blocks [x] [y] = new QGraphicsRectItem (x*CONFIGURATION::CONSOLE_FAKTOR_GUI, y*CONFIGURATION::CONSOLE_FAKTOR_GUI,
                                                    CONFIGURATION::CONSOLE_FAKTOR_GUI, CONFIGURATION::CONSOLE_FAKTOR_GUI);
            blocks [x] [y]->setBrush(Qt::white);
            blocks [x] [y]->setPen(QPen(Qt::black));
            qgs_spielfeld->addItem(blocks[x][y]);
        }
    ui->graphicsView_Spielfeld->setScene(qgs_spielfeld);

    //-------------------------------------------------------------------------------------------------------
    //Stein Legende - Vorbereitung - Zeichnen über legende_blocks [16] [4]
    //-------------------------------------------------------------------------------------------------------
    QGraphicsScene* qgs_steine = new QGraphicsScene(0,0,4*3*CONFIGURATION::CONSOLE_FAKTOR_GUI-20,
                                             4*CONFIGURATION::CONSOLE_FAKTOR_GUI, ui->graphicsView_Spielstein);
    qgs_steine->setBackgroundBrush(QBrush(Qt::lightGray, Qt::SolidPattern));
    QGraphicsRectItem* legende_blocks [16] [4] ;
    for (int x = 0; x < 16; x++)
        for (int y = 0; y < 4; y++)
        {
            legende_blocks [x] [y] = new QGraphicsRectItem (x*(CONFIGURATION::CONSOLE_FAKTOR_GUI-10), y*(CONFIGURATION::CONSOLE_FAKTOR_GUI-10),
                                                    CONFIGURATION::CONSOLE_FAKTOR_GUI-10, CONFIGURATION::CONSOLE_FAKTOR_GUI-10);
            legende_blocks [x] [y]->setBrush(Qt::white);
            legende_blocks [x] [y]->setPen(QPen(Qt::black));
            qgs_steine->addItem(legende_blocks[x][y]);
        }
    ui->graphicsView_Spielstein->setScene(qgs_steine);

    //-------------------------------------------------------------------------------------------------------
    //Initialisieren der Aufgabe
    //-------------------------------------------------------------------------------------------------------
    Aufgabe* aufgabe = new Aufgabe();
    aufgabe->initSchwer();

    //Verstecke Button 4 falls nicht benötigt.
    if (aufgabe->steine.size() <= CONFIGURATION::SPIELSTEIN_4_INDEX)
        ui->radioButton_stein4->hide();
    ui->radioButton_stein1->setChecked(true);

    //-------------------------------------------------------------------------------------------------------
    //Zeichne-Funktionen für die Stein-Legende - Zeichnet über legende_blocks - Jeder Stein 4x4 Felder
    // Anmerkung: Die Legende verwendet stets die Ausgangsposition, also die Positionen aus felder [0][0]
    //-------------------------------------------------------------------------------------------------------
    //Aufgabe 6c)
    auto zeichneLegende = [=] (bool visible)
    {
        for(int i = 0; i < aufgabe->steine.size(); ++i){
            for(Position p : aufgabe->steine.at(i)->felder[0][0]){
                if(visible) legende_blocks[p.getX() + i*4][p.getY()]->setBrush(Cursor::QtFarbwandler(aufgabe->steine.at(i)->getColour()));
                else legende_blocks[p.getX() + i*4][p.getY()]->setBrush(Qt::white);
            }
        }
    };

    //-------------------------------------------------------------------------------------------------------
    //Zeichne-Funktionen für Steine in die Spielfläche - Zeichnet über blocks
    //Anmerkung: Ist visible false wird der Stein in weiß gezeichnet, sonst in original Farbe
    //-------------------------------------------------------------------------------------------------------
    //Aufgabe 6d)
    auto zeichneStein = [=] (Spielstein* stein_ptr, bool visible)
    {
          for (Position p : *stein_ptr->getPositionen())
          {
              int x = stein_ptr->getX() + p.getX();
              int y = stein_ptr->getY() + p.getY();

              if(visible)
                  blocks[x][y]->setBrush(Cursor::QtFarbwandler(stein_ptr->getColour()));
              else
                  blocks[x][y]->setBrush(Qt::white);
          }

    };

    //-------------------------------------------------------------------------------------------------------
    //Zeichne-Funktionen der Aufgabe in die Spielfläche - Zeichnet über Funktion zeichneStein
    //Anmerkung 1: Es wird mit dem Lösungstein begonnen, danach werden alle anderen Steine gezeichnet,
    //             wobei der aktive Stein als letztes gezeichnet wird damit er "oben" liegt.
    //Anmerkung: Ist visible false werden alle Steine in weiß gezeichnet == gelöscht, sonst in original Farbe
    //-------------------------------------------------------------------------------------------------------
    //Aufgabe 6e)
    auto zeichneAufgabe = [=] (bool visible)
    {
            zeichneStein(aufgabe->loesung_ptr,visible);

            for (Spielstein* s : aufgabe->steine)
            {
                zeichneStein(s,visible);
            }

            zeichneStein(aufgabe->steine[aufgabe->aktiverSpielstein],visible);


   };

    //-------------------------------------------------------------------------------------------------------
    // Prüft, ob die Aufgabe gelöst ist. Falls ja, dann ...
    // Funktion:: Zeichnet die aktuelle-Aufgbe und Legende weiß, erstellt eine neue Aufgabe,
    //            zeichnet die neue Aufgae (Legende und Spielsteine). Abschließend wird noch Stein 1 angewählt.
    //-------------------------------------------------------------------------------------------------------
    //Aufgabe 6f)
    auto geloest = [=] ()
    {
           if(aufgabe->geloest())
           {
               zeichneAufgabe(false);
               zeichneLegende(false);

               aufgabe->initSchwer();

               zeichneLegende(true);
               zeichneAufgabe(true);
               ui->radioButton_stein1->setChecked(true);
           }

    };
    //____________________ AB HIER SPIEL UND INDIVIDUELLES SETUP

    //-------------------------------------------------------------------------------------------------------
    // Zeichnet die Aufgabe weiß, versetzt den Spielstein, zeichnet die Aufgabe neu und prüft anschließend,
    // ob die Aufgabe geloest ist.
    //-------------------------------------------------------------------------------------------------------
    //Aufgabe 6g)
    auto bewegen = [=] (int richtung)
    {
              zeichneAufgabe(false);
              aufgabe->steine[aufgabe->aktiverSpielstein]->bewegen(richtung);
              zeichneAufgabe(true);
              geloest();
    };

    auto rotation_links = [=] ()
    {
              zeichneAufgabe(false);
              aufgabe->steine[aufgabe->aktiverSpielstein]->rotation_links();
              zeichneAufgabe(true);
              geloest();
    };

    auto rotation_rechts = [=] ()
    {
              zeichneAufgabe(false);
              aufgabe->steine[aufgabe->aktiverSpielstein]->rotation_rechts();
              zeichneAufgabe(true);
              geloest();
    };

    auto flip = [=] ()
    {
              zeichneAufgabe(false);
              aufgabe->steine[aufgabe->aktiverSpielstein]->flip();
              zeichneAufgabe(true);
              geloest();
    };



    zeichneLegende(true);
    zeichneAufgabe(true);

    //-------------------------------------------------------------------------------------------------------
    // Verbinden der interaktiven grafischen Elemente zur Steinmanipulation
    //-------------------------------------------------------------------------------------------------------
    connect (ui->pushButton_bewegen_hoch, &QPushButton::clicked, [=] ()
    {

                bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH);

    });
    connect (ui->pushButton_bewegen_runter, &QPushButton::clicked, [=] ()
    {
                bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER);
    });
    connect (ui->pushButton_bewegen_links, &QPushButton::clicked, [=] ()
    {
                bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS);
    });
    connect (ui->pushButton_bewegen_rechts, &QPushButton::clicked, [=] ()
    {
                bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS);
    });
    connect (ui->pushButton_drehen_rechts, &QPushButton::clicked, [=] ()
    {
                 rotation_rechts();

    });
    connect (ui->pushButton_drehen_links, &QPushButton::clicked, [=] ()
    {
                 rotation_links();

    });
    connect (ui->pushButton_flip, &QPushButton::clicked, [=] ()
    {
                 flip();

    });
    connect (ui->pushButton_reset, &QPushButton::clicked, [=] ()
    {
        zeichneAufgabe(false);

        aufgabe->getSpielstein(0)->position.setX(CONFIGURATION::SPIELFELD_STEIN1_X);
        aufgabe->getSpielstein(0)->position.setY(CONFIGURATION::SPIELFELD_STEIN1_Y);

        aufgabe->getSpielstein(1)->position.setX(CONFIGURATION::SPIELFELD_STEIN2_X);
        aufgabe->getSpielstein(1)->position.setY(CONFIGURATION::SPIELFELD_STEIN2_Y);

        aufgabe->getSpielstein(2)->position.setX(CONFIGURATION::SPIELFELD_STEIN3_X);
        aufgabe->getSpielstein(2)->position.setY(CONFIGURATION::SPIELFELD_STEIN3_Y);

        aufgabe->getSpielstein(3)->position.setX(CONFIGURATION::SPIELFELD_STEIN4_X);
        aufgabe->getSpielstein(3)->position.setY(CONFIGURATION::SPIELFELD_STEIN4_Y);

        zeichneAufgabe(true);
    });

    connect (ui->pushButton_naechste, &QPushButton::clicked, [=] ()
    {
        zeichneAufgabe(false);
        zeichneLegende(false);

        aufgabe->initSchwer();

        zeichneAufgabe(true);
        zeichneLegende(true);
    });
    //-------------------------------------------------------------------------------------------------------
    // Verbinden der QRadioButtons zur Steinauswahl
    //-------------------------------------------------------------------------------------------------------
    connect (ui->radioButton_stein1, &QRadioButton::clicked, [=] ()
    {
                aufgabe->aktiverSpielstein = 0;
    });
    connect (ui->radioButton_stein2, &QRadioButton::clicked, [=] ()
    {
                aufgabe->aktiverSpielstein = 1;
    });
    connect (ui->radioButton_stein3, &QRadioButton::clicked, [=] ()
    {
                aufgabe->aktiverSpielstein = 2;

    });
    connect (ui->radioButton_stein4, &QRadioButton::clicked, [=] ()
    {
                aufgabe->aktiverSpielstein = 3;

    });


}

MainWindow::~MainWindow()
{
    delete ui;
}

