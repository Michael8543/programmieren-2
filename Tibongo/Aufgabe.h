#ifndef AUFGABE_H_
#define AUFGABE_H_

#include <iostream>
#include "Spielstein.h"


class Aufgabe
{
private:
	std::vector<Spielstein*> steine;

public:
	friend class Spiel;
    friend class MainWindow;

    Spielstein* loesung_ptr;
    int aktiverSpielstein = 0;

	Spielstein* getSpielstein(unsigned int index);
	bool geloest();
	void zeichne();
	void loesche();
    void initSchwer();


	Aufgabe(Spielstein* stein1_ptr, Spielstein* stein2_ptr, Spielstein* stein3_ptr, Spielstein* loesung_ptr);	
    Aufgabe() {};
    ~Aufgabe();
};
#endif /* AUFGABE_H_ */
