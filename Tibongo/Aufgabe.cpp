#include <iostream>

#include "Spielstein.h"
#include "Aufgabe.h"
#include "quadrupel.h"
#include "Position.h"

Aufgabe::Aufgabe(Spielstein* stein1_ptr, Spielstein* stein2_ptr, Spielstein* stein3_ptr, Spielstein* loesung_ptr)
{
    steine.push_back(stein1_ptr);
    steine.push_back(stein2_ptr);
    steine.push_back(stein3_ptr);

    this->loesung_ptr = loesung_ptr;
}

Aufgabe::~Aufgabe(){

}

Spielstein* Aufgabe::getSpielstein(unsigned int index)
{
    return steine.at(index);
}

bool Aufgabe::geloest()
{
    bool temp = false;
    // Äußere Schleife durchläuft alle Positionen des Loesungssteins
    for (Position p : *loesung_ptr->getPositionen())
    {
        temp = false;
        // Innere Schleife Ruft alle Innerhalb() Funktionen aller Steine auf
        for (int j = 0; j < steine.size(); j++)
        {
            if (getSpielstein(j)->innerhalb(loesung_ptr->position + p))
                temp = true;
        }
        if (!temp) return false;
    }
    return true;
}


void Aufgabe::zeichne() {
    loesung_ptr->zeichne();
    for (Spielstein* spielstein : steine) {
        spielstein->zeichne();
    }
}


void Aufgabe::loesche() {
    loesung_ptr->loesche();
    for (Spielstein* spielstein : steine) {
        spielstein->loesche();
    }
}

void Aufgabe::initSchwer()
{
    aktiverSpielstein = 0;

    std::vector<Quadrupel> quadrupels;

    quadrupels.push_back(Quadrupel(2, 3, 10, 4));
    quadrupels.push_back(Quadrupel(12, 0, 2, 8));
    quadrupels.push_back(Quadrupel(5, 9, 8, 1));
    quadrupels.push_back(Quadrupel(8, 6, 0, 3));
    quadrupels.push_back(Quadrupel(3, 5, 4, 8));
    quadrupels.push_back(Quadrupel(10, 7, 3, 2));
    int choice = rand() % 6;

    steine.clear();

    auto *stein_ptr1 = new Spielstein(quadrupels.at(choice).stein1);
    stein_ptr1->position.setX(CONFIGURATION::SPIELFELD_STEIN1_X);
    stein_ptr1->position.setY(CONFIGURATION::SPIELFELD_STEIN1_Y);
    steine.push_back(stein_ptr1);

    auto *stein_ptr2 = new Spielstein(quadrupels.at(choice).stein2);
    stein_ptr2->position.setX(CONFIGURATION::SPIELFELD_STEIN2_X);
    stein_ptr2->position.setY(CONFIGURATION::SPIELFELD_STEIN2_Y);
    steine.push_back(stein_ptr2);

    auto *stein_ptr3 = new Spielstein(quadrupels.at(choice).stein3);
    stein_ptr3->position.setX(CONFIGURATION::SPIELFELD_STEIN3_X);
    stein_ptr3->position.setY(CONFIGURATION::SPIELFELD_STEIN3_Y);
    steine.push_back(stein_ptr3);

    auto *stein_ptr4 = new Spielstein(quadrupels.at(choice).stein4);
    stein_ptr4->position.setX(CONFIGURATION::SPIELFELD_STEIN4_X);
    stein_ptr4->position.setY(CONFIGURATION::SPIELFELD_STEIN4_Y);
    steine.push_back(stein_ptr4);

    auto *loesung = new Spielstein(SPIELSTEIN_LOESUNG);
    loesung->position.setX(CONFIGURATION::SPIELFELD_LOESUNG_X);
    loesung->position.setY(CONFIGURATION::SPIELFELD_LOESUNG_Y);
    this->loesung_ptr = loesung;

}


