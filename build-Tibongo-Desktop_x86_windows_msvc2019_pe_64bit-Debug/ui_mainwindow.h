/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.3.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGraphicsView *graphicsView_Spielfeld;
    QGraphicsView *graphicsView_Spielstein;
    QPushButton *pushButton_drehen_links;
    QPushButton *pushButton_bewegen_hoch;
    QPushButton *pushButton_drehen_rechts;
    QPushButton *pushButton_flip;
    QPushButton *pushButton_bewegen_links;
    QPushButton *pushButton_bewegen_rechts;
    QPushButton *pushButton_bewegen_runter;
    QPushButton *pushButton_reset;
    QPushButton *pushButton_naechste;
    QLabel *label_punkte;
    QLabel *label_eingaben;
    QLabel *label_zeit;
    QLCDNumber *lcdNumber_punkte;
    QLCDNumber *lcdNumber_eingaben;
    QLCDNumber *lcdNumber_zeit;
    QRadioButton *radioButton_stein2;
    QRadioButton *radioButton_stein3;
    QRadioButton *radioButton_stein4;
    QLabel *label_logo;
    QRadioButton *radioButton_stein1;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1024, 768);
        MainWindow->setIconSize(QSize(1024, 768));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        graphicsView_Spielfeld = new QGraphicsView(centralwidget);
        graphicsView_Spielfeld->setObjectName(QString::fromUtf8("graphicsView_Spielfeld"));
        graphicsView_Spielfeld->setGeometry(QRect(30, 20, 500, 500));
        graphicsView_Spielstein = new QGraphicsView(centralwidget);
        graphicsView_Spielstein->setObjectName(QString::fromUtf8("graphicsView_Spielstein"));
        graphicsView_Spielstein->setGeometry(QRect(580, 230, 400, 170));
        pushButton_drehen_links = new QPushButton(centralwidget);
        pushButton_drehen_links->setObjectName(QString::fromUtf8("pushButton_drehen_links"));
        pushButton_drehen_links->setGeometry(QRect(570, 420, 130, 31));
        QFont font;
        font.setPointSize(12);
        pushButton_drehen_links->setFont(font);
        pushButton_bewegen_hoch = new QPushButton(centralwidget);
        pushButton_bewegen_hoch->setObjectName(QString::fromUtf8("pushButton_bewegen_hoch"));
        pushButton_bewegen_hoch->setGeometry(QRect(720, 420, 130, 31));
        pushButton_bewegen_hoch->setFont(font);
        pushButton_drehen_rechts = new QPushButton(centralwidget);
        pushButton_drehen_rechts->setObjectName(QString::fromUtf8("pushButton_drehen_rechts"));
        pushButton_drehen_rechts->setGeometry(QRect(860, 420, 130, 31));
        pushButton_drehen_rechts->setFont(font);
        pushButton_flip = new QPushButton(centralwidget);
        pushButton_flip->setObjectName(QString::fromUtf8("pushButton_flip"));
        pushButton_flip->setGeometry(QRect(720, 450, 130, 31));
        pushButton_flip->setFont(font);
        pushButton_bewegen_links = new QPushButton(centralwidget);
        pushButton_bewegen_links->setObjectName(QString::fromUtf8("pushButton_bewegen_links"));
        pushButton_bewegen_links->setGeometry(QRect(570, 450, 130, 31));
        pushButton_bewegen_links->setFont(font);
        pushButton_bewegen_rechts = new QPushButton(centralwidget);
        pushButton_bewegen_rechts->setObjectName(QString::fromUtf8("pushButton_bewegen_rechts"));
        pushButton_bewegen_rechts->setGeometry(QRect(860, 450, 130, 31));
        pushButton_bewegen_rechts->setFont(font);
        pushButton_bewegen_runter = new QPushButton(centralwidget);
        pushButton_bewegen_runter->setObjectName(QString::fromUtf8("pushButton_bewegen_runter"));
        pushButton_bewegen_runter->setGeometry(QRect(720, 480, 130, 31));
        pushButton_bewegen_runter->setFont(font);
        pushButton_reset = new QPushButton(centralwidget);
        pushButton_reset->setObjectName(QString::fromUtf8("pushButton_reset"));
        pushButton_reset->setGeometry(QRect(570, 480, 130, 31));
        pushButton_reset->setFont(font);
        pushButton_naechste = new QPushButton(centralwidget);
        pushButton_naechste->setObjectName(QString::fromUtf8("pushButton_naechste"));
        pushButton_naechste->setGeometry(QRect(860, 480, 130, 31));
        pushButton_naechste->setFont(font);
        label_punkte = new QLabel(centralwidget);
        label_punkte->setObjectName(QString::fromUtf8("label_punkte"));
        label_punkte->setGeometry(QRect(580, 10, 300, 60));
        QFont font1;
        font1.setPointSize(12);
        font1.setUnderline(false);
        label_punkte->setFont(font1);
        label_eingaben = new QLabel(centralwidget);
        label_eingaben->setObjectName(QString::fromUtf8("label_eingaben"));
        label_eingaben->setGeometry(QRect(580, 140, 300, 60));
        label_eingaben->setFont(font);
        label_zeit = new QLabel(centralwidget);
        label_zeit->setObjectName(QString::fromUtf8("label_zeit"));
        label_zeit->setGeometry(QRect(580, 70, 300, 60));
        label_zeit->setFont(font);
        lcdNumber_punkte = new QLCDNumber(centralwidget);
        lcdNumber_punkte->setObjectName(QString::fromUtf8("lcdNumber_punkte"));
        lcdNumber_punkte->setGeometry(QRect(890, 30, 64, 23));
        lcdNumber_eingaben = new QLCDNumber(centralwidget);
        lcdNumber_eingaben->setObjectName(QString::fromUtf8("lcdNumber_eingaben"));
        lcdNumber_eingaben->setGeometry(QRect(890, 100, 64, 23));
        lcdNumber_zeit = new QLCDNumber(centralwidget);
        lcdNumber_zeit->setObjectName(QString::fromUtf8("lcdNumber_zeit"));
        lcdNumber_zeit->setGeometry(QRect(890, 170, 64, 23));
        radioButton_stein2 = new QRadioButton(centralwidget);
        radioButton_stein2->setObjectName(QString::fromUtf8("radioButton_stein2"));
        radioButton_stein2->setGeometry(QRect(690, 360, 80, 25));
        radioButton_stein3 = new QRadioButton(centralwidget);
        radioButton_stein3->setObjectName(QString::fromUtf8("radioButton_stein3"));
        radioButton_stein3->setGeometry(QRect(790, 360, 80, 25));
        radioButton_stein4 = new QRadioButton(centralwidget);
        radioButton_stein4->setObjectName(QString::fromUtf8("radioButton_stein4"));
        radioButton_stein4->setGeometry(QRect(890, 360, 80, 25));
        label_logo = new QLabel(centralwidget);
        label_logo->setObjectName(QString::fromUtf8("label_logo"));
        label_logo->setGeometry(QRect(180, 550, 691, 81));
        radioButton_stein1 = new QRadioButton(centralwidget);
        radioButton_stein1->setObjectName(QString::fromUtf8("radioButton_stein1"));
        radioButton_stein1->setGeometry(QRect(590, 360, 80, 25));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1024, 22));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        pushButton_drehen_links->setText(QCoreApplication::translate("MainWindow", "Drehen Links", nullptr));
        pushButton_bewegen_hoch->setText(QCoreApplication::translate("MainWindow", "Hoch", nullptr));
        pushButton_drehen_rechts->setText(QCoreApplication::translate("MainWindow", "Drehen Rechts", nullptr));
        pushButton_flip->setText(QCoreApplication::translate("MainWindow", "Flip", nullptr));
        pushButton_bewegen_links->setText(QCoreApplication::translate("MainWindow", "Links", nullptr));
        pushButton_bewegen_rechts->setText(QCoreApplication::translate("MainWindow", "Rechts", nullptr));
        pushButton_bewegen_runter->setText(QCoreApplication::translate("MainWindow", "Runter", nullptr));
        pushButton_reset->setText(QCoreApplication::translate("MainWindow", "Reset", nullptr));
        pushButton_naechste->setText(QCoreApplication::translate("MainWindow", "N\303\244chste", nullptr));
        label_punkte->setText(QCoreApplication::translate("MainWindow", "<html><head/><body><p><span style=\" font-size:16pt; font-weight:700;\">Punkte</span></p></body></html>", nullptr));
        label_eingaben->setText(QCoreApplication::translate("MainWindow", "Eingaben", nullptr));
        label_zeit->setText(QCoreApplication::translate("MainWindow", "Zeit", nullptr));
        radioButton_stein2->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
        radioButton_stein3->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
        radioButton_stein4->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
        label_logo->setText(QCoreApplication::translate("MainWindow", "TextLabel", nullptr));
        radioButton_stein1->setText(QCoreApplication::translate("MainWindow", "RadioButton", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
