#include "Spielstein.h"
#include "Cursor.h"
#include <vector>

void Spielstein::init() {
	//IHR CODE / TEILVORLAGE
    switch (typ) {
    case SPIELSTEIN_Z2: {
        name = "Z2";
        farbe = HINTERGRUND_BLAU;
        felder[0][0].push_back(Position(2, 0));//__#_
        felder[0][0].push_back(Position(1, 1));//_##_
        felder[0][0].push_back(Position(2, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//____

        felder[0][1].push_back(Position(1, 1));//____
        felder[0][1].push_back(Position(2, 1));//_##_
        felder[0][1].push_back(Position(2, 2));//__##
        felder[0][1].push_back(Position(3, 2));//____

        felder[0][2].push_back(Position(2, 0));//__#_
        felder[0][2].push_back(Position(1, 1));//_##_
        felder[0][2].push_back(Position(2, 1));//_#__
        felder[0][2].push_back(Position(1, 2));//____

        felder[0][3].push_back(Position(1, 1));//____
        felder[0][3].push_back(Position(2, 1));//_##_
        felder[0][3].push_back(Position(2, 2));//__##
        felder[0][3].push_back(Position(3, 2));//____

        felder[1][0].push_back(Position(1, 0));//_#__
        felder[1][0].push_back(Position(1, 1));//_##_
        felder[1][0].push_back(Position(2, 1));//__#_
        felder[1][0].push_back(Position(2, 2));//____

        felder[1][1].push_back(Position(2, 1));//____
        felder[1][1].push_back(Position(3, 1));//__##
        felder[1][1].push_back(Position(1, 2));//_##_
        felder[1][1].push_back(Position(2, 2));//____

        felder[1][2].push_back(Position(1, 0));//_#__
        felder[1][2].push_back(Position(1, 1));//_##_
        felder[1][2].push_back(Position(2, 1));//__#_
        felder[1][2].push_back(Position(2, 2));//____

        felder[1][3].push_back(Position(2, 1));//____
        felder[1][3].push_back(Position(3, 1));//__##
        felder[1][3].push_back(Position(1, 2));//_##_
        felder[1][3].push_back(Position(2, 2));//____
        break;
    }
    case SPIELSTEIN_Z3: {
        name = "Z3";
        farbe = HINTERGRUND_CYAN;

        felder[0][0].push_back(Position(3, 0));//___#_
        felder[0][0].push_back(Position(1, 1));//_###_
        felder[0][0].push_back(Position(2, 1));//_#___
        felder[0][0].push_back(Position(3, 1));
        felder[0][0].push_back(Position(1, 2));

        felder[0][1].push_back(Position(0, 0));
        felder[0][1].push_back(Position(1, 2));
        felder[0][1].push_back(Position(1, 1));
        felder[0][1].push_back(Position(1, 0));
        felder[0][1].push_back(Position(2, 2));
        felder[0][2].push_back(Position(0, 2));
        felder[0][2].push_back(Position(2, 1));
        felder[0][2].push_back(Position(1, 1));
        felder[0][2].push_back(Position(0, 1));
        felder[0][2].push_back(Position(2, 0));
        felder[0][3].push_back(Position(2, 2));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(1, 1));
        felder[0][3].push_back(Position(1, 2));
        felder[0][3].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 0));
        felder[1][0].push_back(Position(2, 1));
        felder[1][0].push_back(Position(1, 1));
        felder[1][0].push_back(Position(0, 1));
        felder[1][0].push_back(Position(2, 2));
        felder[1][1].push_back(Position(0, 2));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(1, 1));
        felder[1][1].push_back(Position(1, 2));
        felder[1][1].push_back(Position(2, 0));
        felder[1][2].push_back(Position(2, 2));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(1, 1));
        felder[1][2].push_back(Position(2, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][3].push_back(Position(2, 0));
        felder[1][3].push_back(Position(1, 2));
        felder[1][3].push_back(Position(1, 1));
        felder[1][3].push_back(Position(1, 0));
        felder[1][3].push_back(Position(0, 2));
        break;
    }
    case SPIELSTEIN_L2: {
        name = "L2";
        farbe = HINTERGRUND_GRUEN;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_##_
        felder[0][0].push_back(Position(2, 1));

        felder[0][1].push_back(Position(0, 1));
        felder[0][1].push_back(Position(1, 1));
        felder[0][1].push_back(Position(1, 0));
        felder[0][2].push_back(Position(1, 1));
        felder[0][2].push_back(Position(1, 0));
        felder[0][2].push_back(Position(0, 0));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(0, 0));
        felder[0][3].push_back(Position(0, 1));
        felder[1][0].push_back(Position(1, 0));
        felder[1][0].push_back(Position(1, 1));
        felder[1][0].push_back(Position(0, 1));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(1, 1));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][2].push_back(Position(1, 0));
        felder[1][3].push_back(Position(1, 1));
        felder[1][3].push_back(Position(0, 1));
        felder[1][3].push_back(Position(0, 0));
        break;
    }
    case SPIELSTEIN_L3: {
        name = "L3";
        farbe = HINTERGRUND_HELL_GRUEN;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//_##_
        felder[0][0].push_back(Position(2, 2));

        felder[0][1].push_back(Position(0, 1));
        felder[0][1].push_back(Position(1, 1));
        felder[0][1].push_back(Position(2, 1));
        felder[0][1].push_back(Position(2, 0));
        felder[0][2].push_back(Position(1, 2));
        felder[0][2].push_back(Position(1, 1));
        felder[0][2].push_back(Position(1, 0));
        felder[0][2].push_back(Position(0, 0));
        felder[0][3].push_back(Position(2, 0));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(0, 0));
        felder[0][3].push_back(Position(0, 1));
        felder[1][0].push_back(Position(1, 0));
        felder[1][0].push_back(Position(1, 1));
        felder[1][0].push_back(Position(1, 2));
        felder[1][0].push_back(Position(0, 2));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(2, 0));
        felder[1][1].push_back(Position(2, 1));
        felder[1][2].push_back(Position(0, 2));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][2].push_back(Position(1, 0));
        felder[1][3].push_back(Position(2, 1));
        felder[1][3].push_back(Position(1, 1));
        felder[1][3].push_back(Position(0, 1));
        felder[1][3].push_back(Position(0, 0));
        break;
    }
    case SPIELSTEIN_L4: {
        name = "L4";
        farbe = HINTERGRUND_HELL_ROT;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//_#__
        felder[0][0].push_back(Position(1, 3));//_##_
        felder[0][0].push_back(Position(2, 3));

        felder[0][1].push_back(Position(3, 1));//___#
        felder[0][1].push_back(Position(2, 1));//####
        felder[0][1].push_back(Position(1, 1));
        felder[0][1].push_back(Position(0, 1));
        felder[0][1].push_back(Position(3, 0));

        felder[0][2].push_back(Position(1, 2));//##__
        felder[0][2].push_back(Position(1, 1));//_#__
        felder[0][2].push_back(Position(1, 0));//_#__
        felder[0][2].push_back(Position(0, 0));//_#__
        felder[0][2].push_back(Position(1, 3));//_____

        felder[0][3].push_back(Position(3, 0));//####
        felder[0][3].push_back(Position(2, 0));//#___
        felder[0][3].push_back(Position(1, 0));//
        felder[0][3].push_back(Position(0, 0));//
        felder[0][3].push_back(Position(0, 1));//
        


        felder[1][0].push_back(Position(1, 0));//_#
        felder[1][0].push_back(Position(1, 1));//_#
        felder[1][0].push_back(Position(1, 2));//_#
        felder[1][0].push_back(Position(1, 3));//##
        felder[1][0].push_back(Position(0, 3));
        
        felder[1][1].push_back(Position(0, 0));//####
        felder[1][1].push_back(Position(1, 0));//___#
        felder[1][1].push_back(Position(2, 0));
        felder[1][1].push_back(Position(3, 0));
        felder[1][1].push_back(Position(3, 1));
        
        felder[1][2].push_back(Position(0, 3));//##
        felder[1][2].push_back(Position(0, 2));//#
        felder[1][2].push_back(Position(0, 1));//#
        felder[1][2].push_back(Position(0, 0));//#
        felder[1][2].push_back(Position(1, 0));
        
        felder[1][3].push_back(Position(3, 1));//#
        felder[1][3].push_back(Position(2, 1));//####
        felder[1][3].push_back(Position(1, 1));
        felder[1][3].push_back(Position(0, 1));
        felder[1][3].push_back(Position(0, 0));

        break;
    }
    case SPIELSTEIN_I2: {
        name = "I2";
        farbe = HINTERGRUND_CYAN;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_#__

        felder[0][1].push_back(Position(0, 0));
        felder[0][1].push_back(Position(1, 0));
        felder[0][2].push_back(Position(0, 1));
        felder[0][2].push_back(Position(0, 0));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 1));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][3].push_back(Position(1, 0));
        felder[1][3].push_back(Position(0, 0));
        break;
    }
    case SPIELSTEIN_I3: {
        name = "I3";
        farbe = HINTERGRUND_HELL_BLAU;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//_#__

        felder[0][1].push_back(Position(0, 0));
        felder[0][1].push_back(Position(1, 0));
        felder[0][1].push_back(Position(2, 0));
        felder[0][2].push_back(Position(0, 2));
        felder[0][2].push_back(Position(0, 1));
        felder[0][2].push_back(Position(0, 0));
        felder[0][3].push_back(Position(2, 0));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 1));
        felder[1][0].push_back(Position(0, 2));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(2, 0));
        felder[1][2].push_back(Position(0, 2));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][3].push_back(Position(2, 0));
        felder[1][3].push_back(Position(1, 0));
        felder[1][3].push_back(Position(0, 0));
        break;
    }
    case SPIELSTEIN_I4: {
        name = "I4";
        farbe = HINTERGRUND_GRUEN;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//_#__
        felder[0][0].push_back(Position(1, 3));//_#__

        felder[0][1].push_back(Position(0, 0));
        felder[0][1].push_back(Position(1, 0));
        felder[0][1].push_back(Position(2, 0));
        felder[0][1].push_back(Position(3, 0));
        felder[0][2].push_back(Position(0, 3));
        felder[0][2].push_back(Position(0, 2));
        felder[0][2].push_back(Position(0, 1));
        felder[0][2].push_back(Position(0, 0));
        felder[0][3].push_back(Position(3, 0));
        felder[0][3].push_back(Position(2, 0));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 0));
        felder[1][0].push_back(Position(0, 1));
        felder[1][0].push_back(Position(0, 2));
        felder[1][0].push_back(Position(0, 3));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(2, 0));
        felder[1][1].push_back(Position(3, 0));
        felder[1][2].push_back(Position(0, 3));
        felder[1][2].push_back(Position(0, 2));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][3].push_back(Position(3, 0));
        felder[1][3].push_back(Position(2, 0));
        felder[1][3].push_back(Position(1, 0));
        felder[1][3].push_back(Position(0, 0));
        break;
    }
    case SPIELSTEIN_Q: {

        break;
    }
    case SPIELSTEIN_QPLUS: //GECHECKT
    {

        break;
    }
    case SPIELSTEIN_T: {
        name = "T";
        farbe = HINTERGRUND_BLAU;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_##_
        felder[0][0].push_back(Position(2, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//____

        felder[0][1].push_back(Position(0, 1));
        felder[0][1].push_back(Position(1, 1));
        felder[0][1].push_back(Position(1, 0));
        felder[0][1].push_back(Position(2, 1));
        felder[0][2].push_back(Position(1, 2));
        felder[0][2].push_back(Position(1, 1));
        felder[0][2].push_back(Position(0, 1));
        felder[0][2].push_back(Position(1, 0));
        felder[0][3].push_back(Position(2, 0));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(1, 1));
        felder[0][3].push_back(Position(0, 0));
        felder[1][0].push_back(Position(1, 0));
        felder[1][0].push_back(Position(1, 1));
        felder[1][0].push_back(Position(0, 1));
        felder[1][0].push_back(Position(1, 2));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(1, 1));
        felder[1][1].push_back(Position(2, 0));
        felder[1][2].push_back(Position(0, 2));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(1, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][3].push_back(Position(2, 1));
        felder[1][3].push_back(Position(1, 1));
        felder[1][3].push_back(Position(1, 0));
        felder[1][3].push_back(Position(0, 1));
        break;
    }
    case SPIELSTEIN_TPLUS: {
        name = "TPLUS";
        farbe = HINTERGRUND_ROT;
        felder[0][0].push_back(Position(1, 0));//_#__
        felder[0][0].push_back(Position(1, 1));//_##_
        felder[0][0].push_back(Position(2, 1));//_#__
        felder[0][0].push_back(Position(1, 2));//_#__
        felder[0][0].push_back(Position(1, 3));

        felder[0][1].push_back(Position(0, 1));
        felder[0][1].push_back(Position(1, 1));
        felder[0][1].push_back(Position(1, 0));
        felder[0][1].push_back(Position(2, 1));
        felder[0][1].push_back(Position(3, 1));
        felder[0][2].push_back(Position(1, 3));
        felder[0][2].push_back(Position(1, 2));
        felder[0][2].push_back(Position(0, 2));
        felder[0][2].push_back(Position(1, 1));
        felder[0][2].push_back(Position(1, 0));
        felder[0][3].push_back(Position(3, 0));
        felder[0][3].push_back(Position(2, 0));
        felder[0][3].push_back(Position(2, 1));
        felder[0][3].push_back(Position(1, 0));
        felder[0][3].push_back(Position(0, 0));
        felder[1][0].push_back(Position(1, 0));
        felder[1][0].push_back(Position(1, 1));
        felder[1][0].push_back(Position(0, 1));
        felder[1][0].push_back(Position(1, 2));
        felder[1][0].push_back(Position(1, 3));
        felder[1][1].push_back(Position(0, 0));
        felder[1][1].push_back(Position(1, 0));
        felder[1][1].push_back(Position(1, 1));
        felder[1][1].push_back(Position(2, 0));
        felder[1][1].push_back(Position(3, 0));
        felder[1][2].push_back(Position(0, 3));
        felder[1][2].push_back(Position(0, 2));
        felder[1][2].push_back(Position(1, 2));
        felder[1][2].push_back(Position(0, 1));
        felder[1][2].push_back(Position(0, 0));
        felder[1][3].push_back(Position(3, 1));
        felder[1][3].push_back(Position(2, 1));
        felder[1][3].push_back(Position(2, 0));
        felder[1][3].push_back(Position(1, 1));
        felder[1][3].push_back(Position(0, 1));
        break;
    }
    case SPIELSTEIN_FREI: {
        name = "FREI";
        farbe = HINTERGRUND_HELL_GRAU;
        break;
    }
    case SPIELSTEIN_LOESUNG: {
        name = "LOESUNG";
        farbe = HINTERGRUND_HELL_WEISS;
        felder[0][0].push_back(Position(1, 0)); // _###__
        felder[0][0].push_back(Position(2, 0)); // _###__
        felder[0][0].push_back(Position(3, 0)); // _####_
        felder[0][0].push_back(Position(1, 1)); // _###__
        felder[0][0].push_back(Position(2, 1));
        felder[0][0].push_back(Position(3, 1));
        felder[0][0].push_back(Position(1, 2));
        felder[0][0].push_back(Position(2, 2));
        felder[0][0].push_back(Position(3, 2));
        felder[0][0].push_back(Position(4, 2));
        felder[0][0].push_back(Position(1, 3));
        felder[0][0].push_back(Position(2, 3));
        felder[0][0].push_back(Position(3, 3));
        break;
    }
    case SPIELSTEIN_UMRANDUNG:
    {
        name = "UMRANDUNG";
        farbe = HINTERGRUND_HELL_GRAU;
        // Zeilen Y
        for (int y = CONFIGURATION::SPIELFELD_OFFSET_Y; y < CONFIGURATION::SPIELFELD_OFFSET_Y + CONFIGURATION::SPIELFELD_DIMENSION_Y; y++) {
            // Spalte X
            for (int x = CONFIGURATION::SPIELFELD_OFFSET_X; x < CONFIGURATION::SPIELFELD_OFFSET_X + CONFIGURATION::SPIELFELD_DIMENSION_X; x++)
            {
                if (y == CONFIGURATION::SPIELFELD_OFFSET_Y || y == (CONFIGURATION::SPIELFELD_OFFSET_Y + CONFIGURATION::SPIELFELD_DIMENSION_Y - 1))
                    felder[0][0].push_back(Position(x, y));

                else if (x == CONFIGURATION::SPIELFELD_OFFSET_X || x == (CONFIGURATION::SPIELFELD_OFFSET_X + CONFIGURATION::SPIELFELD_DIMENSION_X - 1))
                    felder[0][0].push_back(Position(x, y));
            }
        }
    }
    default:
        break;
    }

}

int Spielstein::getX(){
	return (position.getX());
}

void Spielstein::setX(int x){
	position.setX(x);
}

int Spielstein::getY(){
	return position.getY();
}

void Spielstein::setY(int y) {
    position.setY(y);
}

int Spielstein::getOrientierung(){
    return orientierung;
}

int Spielstein::getSeite(){
    return seite;
}

Position Spielstein::getPos(){
	return position;
}

void Spielstein::setPos(Position pos) {
    this->position = pos;
}

void Spielstein::zeichne() {
    Cursor::setze_Farbe(farbe);
    for (Position p : *getPositionen()) {
        Console::zeichne_punkt(p.getX() + position.getX(), p.getY() + position.getY(), ' ');
    }
    Cursor::setze_Farbe(HINTERGRUND_SCHWARZ);
}

void Spielstein::zeichne(int typ)
{
    for (Position p : *getPositionen()) {
        Console::zeichne_punkt(p.getX() + position.getX(), p.getY() + position.getY(), CONFIGURATION::SPIELFELD_BEGRENZUNG);
    }
}

void Spielstein::loesche() {
    Cursor::setze_Farbe(HINTERGRUND_SCHWARZ);
    for (Position p : *getPositionen()) {
        Console::zeichne_punkt(p.getX() + position.getX(), p.getY() + position.getY(), ' ');
    }
}


void Spielstein::bewegen(int richtung)
{
	switch (richtung)
	{
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH:
		{
			setY(getY()-1);
			break;
		}
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER:
		{
			setY(getY()+1);
			break;
		}
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS:
		{
			setX(getX()+1);
			break;
		}
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS:
		{
			setX(getX()-1);
			break;
		}
		default:
			break;

	}
}
//Inkrementiert das Attribut orientierung um 1, wobei stets Werte zwischen 0 und 3 angenommen werden.
void Spielstein::rotation_links()
{
	orientierung++;
	if (orientierung >= 4)
		orientierung = 0;
}
//Dekrementiert das Attribut orientierung um 1, wobei stets Werte zwischen 0 und 3 angenommen werden.
void Spielstein::rotation_rechts()
{
	orientierung--;
	if (orientierung <= -1)
		orientierung = 3;
}
//Inkrementiert das Attribut seite um 1, wobei stets Werte zwischen 0 und 1 angenommen werden.
void Spielstein::flip()
{
	seite++;
	if (seite >= 2)
		seite = 0;
}

//Gibt den felder Vektor der aktuellen Lage des Spielsteins zur�ck.
const std::vector<Position>* Spielstein::getPositionen() const {
    return &felder[seite][orientierung];
}


//Durchl�uft den felder Vektor des Spielsteins in aktueller Lage.
//Gibt true zur�ck, falls die �bergebene Position innerhalb des Spielsteines liegt.
//Ansonsten wird false zur�ckgegeben.
bool Spielstein::innerhalb(Position pos) const {
    for (Position p : *getPositionen()) {
        if (position + p == pos)
            return true;
    }
    return false;
}


//Gibt war zur�ck, falls sich der aufrufende und der �bergebene Spielstein sich in mindestens einer Position �berlappen.
bool Spielstein::ueberlapp(Spielstein& sp) const {
    for (Position p : *getPositionen()) {
        if (sp.innerhalb(position + p)) return true;
    }
    return false;
}


//Addiert zwei Spielsteine in ihrer aktuellen Lage und gibt die Summe als neuen Spielstein zur�ck.
Spielstein Spielstein::operator+(const Spielstein& rhs) const {

    Spielstein summe(SPIELSTEIN_FREI);

    for (Position p : *this->getPositionen()) 
        summe.felder[0][0].push_back(p + this->position);
    
    for (Position p : *rhs.getPositionen()) 
        summe.felder[0][0].push_back(p + rhs.position);

    return summe;
}

