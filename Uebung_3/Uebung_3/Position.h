#ifndef POSITION
#define POSITION
#include <iostream>
class Position
{
protected:
	int x = 0;
	int y = 0;
public:
	int getX();
	void setX(int x);
	int getY();
	void setY(int y);

	std::string to_string();

	Position();
	Position(int x, int y);
	~Position() {};

	Position operator+ (const Position& rhs) const;
		bool operator==(const Position& rhs) const;

};
#endif
