#include <iostream>

#include "Spielstein.h"
#include "Configuration.h"
#include "Aufgabe.h"
#include "Spiel.h"
#include "Position.h"
#include "SimpleInput.h"
#include <windows.h>

void testSpielsteine()
{
	for (int i = 0; i <= 11; i++)
	{
		Spielstein* stein_ptr = new Spielstein(i);
		stein_ptr->position.setX(i * 6);
		stein_ptr->position.setY(15);
		stein_ptr->zeichne();
	}
}


int main()
{
	Spielstein* s1 = new Spielstein(SPIELSTEIN_T);
	Spielstein* s2 = new Spielstein(SPIELSTEIN_L3);
	Spielstein* s3 = new Spielstein(SPIELSTEIN_L4);
	//Spielstein* ls = new Spielstein(SPIELSTEIN_T);

	Spielstein* ls = new Spielstein(SPIELSTEIN_LOESUNG);

	s1->setX(CONFIGURATION::SPIELFELD_STEIN1_X);
	s1->setY(CONFIGURATION::SPIELFELD_STEIN1_Y);

	s2->setX(CONFIGURATION::SPIELFELD_STEIN2_X);
	s2->setY(CONFIGURATION::SPIELFELD_STEIN2_Y);

	s3->setX(CONFIGURATION::SPIELFELD_STEIN3_X);
	s3->setY(CONFIGURATION::SPIELFELD_STEIN3_Y);

	ls->setX(CONFIGURATION::SPIELFELD_LOESUNG_X - 2);
	ls->setY(CONFIGURATION::SPIELFELD_LOESUNG_Y - 2);

	Aufgabe* TestAufgabe = new Aufgabe(s1, s2, s3, ls);
	TestAufgabe->zeichne();

	Spiel TestSpiel(TestAufgabe);
}
