#include "Position.h"
#include <iostream>
#include <string>


int Position::getX()
{
	return x;
}

void Position::setX(int x)
{
	this->x = x;
	return;
}

int Position::getY()
{
	return y;
}

void Position::setY(int y)
{
	this->y = y;
	return;
}

std::string Position::to_string()
{
	return "Position: x = " + std::to_string(x) + ", y = " + std::to_string(y);
}

Position::Position() : x(0), y(0) { }

Position::Position(int x, int y) : x(x), y(y) { }

//Addiert zwei Positionen indem die Attribute x und y addiert werden.
Position Position::operator+ (const Position& rhs) const {
	Position temp;
	temp.x = this-> x + rhs.x;
	temp.y = this-> y + rhs.y;
	return temp;
}

//Vergleicht zwei Position Instanzen. Gibt true zur�ck, falls x und y beider Instanzen identisch sind, sonst false.
bool Position::operator==(const  Position& rhs) const{
	if(this->x != rhs.x) return false;
	else if(this->y != rhs.y) return false;
	else return true;
}
