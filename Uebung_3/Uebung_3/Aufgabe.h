#ifndef AUFGABE_H_
#define AUFGABE_H_

#include <iostream>
#include "Spielstein.h"


class Aufgabe
{
private:
	std::vector<Spielstein*> steine;

public:
	friend class Spiel;
	Spielstein* loesung_ptr;

	Spielstein* getSpielstein(unsigned int index);
	bool geloest();
	void zeichne();
	void loesche();
	Aufgabe(Spielstein* stein1_ptr, Spielstein* stein2_ptr, Spielstein* stein3_ptr, Spielstein* loesung_ptr);	
	Aufgabe() {  };
};
#endif /* AUFGABE_H_ */
