#include "Spiel.h"
#include "Cursor.h"
#include "Console.h"	
#include "Configuration.h"
#include "Aufgabe.h"
#include "Spielstein.h"
#include "SimpleInput.h"
#include <iostream>
#include <windows.h>

void Spiel::init()
{
	Console::verstecken();
	Spiel::ausgabeRahmen();
	Spiel::ausgabeLegende();
	Spiel::ausgabeSteuerung();
	Spiel::ausgabeLogo();

}

Spiel::Spiel(Aufgabe* aufgabe)
{
	Spiel::aktuelleAufgabe = *aufgabe;
	Spiel::init();
	Spiel::spielSchleife();
}


void Spiel::ausgabeLogo() 
{
	char logo[8][69] =
	{
	("  _|_|_|_|_|  _|  _|_|_|                                            "),
	("      _|          _|    _|    _|_|    _|_|_|      _|_|_|    _|_|    "),
	("      _|      _|  _|_|_|    _|    _|  _|    _|  _|    _|  _|    _|  "),
	("      _|      _|  _|    _|  _|    _|  _|    _|  _|    _|  _|    _|  "),
	("      _|      _|  _|_|_|      _|_|    _|    _|    _|_|_|    _|_|    "),
	("                                                     _|             "),
	("                                                  _|_|              ")
	};
	for (int y = 0; y < 8; y++) {
		for (int x = 0; x < 69; x++)
		{
			Console::zeichne_punkt(CONFIGURATION::SPIELFELD_LOGO_X + x, CONFIGURATION::SPIELFELD_LOGO_Y + y, logo[y][x]);
		}
	}
}

void Spiel::ausgabeRahmen() 
{
	umrandung->zeichne(SPIELSTEIN_UMRANDUNG);
}

void Spiel::ausgabeLegende() {
	for (int i = 0; i < aktuelleAufgabe.steine.size(); ++i) {
		Position temp = aktuelleAufgabe.steine.at(i)->position;
		aktuelleAufgabe.steine.at(i)->position = Position(5 + 6 * i, 20);
		aktuelleAufgabe.steine.at(i)->zeichne();
		aktuelleAufgabe.steine.at(i)->position = temp;
		Cursor::bewegen(6 + 6 * i, 25);
		std::cout << i + 1;
	}
}
void Spiel::ausgabeSteuerung() {
	int y = 2;
	int x = 40;
	Cursor::bewegen(x, ++y);
	std::cout << "##############################";
	Cursor::bewegen(x, ++y);
	std::cout << "# SPIELSTEIN - BEWEGUNG:     #";
	Cursor::bewegen(x, ++y);
	std::cout << "#----------------------------#";
	Cursor::bewegen(x, ++y);
	std::cout << "# W - HOCH                   #";
	Cursor::bewegen(x, ++y);
	std::cout << "# A - LINKS                  #";
	Cursor::bewegen(x, ++y);
	std::cout << "# S - RUNTER                 #";
	Cursor::bewegen(x, ++y);
	std::cout << "# D - RECHTS                 #";
	Cursor::bewegen(x, ++y);
	std::cout << "# Q - RECHTS ROTATION        #";
	Cursor::bewegen(x, ++y);
	std::cout << "# E - LINKS ROTATION         #";
	Cursor::bewegen(x, ++y);
	std::cout << "# F - FLIP                   #";
	Cursor::bewegen(x, ++y);
	std::cout << "# 1, 2, 3, 4 - STEINAUSWAHL  #";
	Cursor::bewegen(x, ++y);
	std::cout << "##############################";
}

void Spiel::spielSchleife()
{
	while (!(Spiel::aktuelleAufgabe.geloest()))
	{
		Spiel::spielzug();
	}

	for (int i = 0; i < 10; i++)
	{
		Sleep(50);
		aktuelleAufgabe.loesche();
		Sleep(50);
		aktuelleAufgabe.zeichne();
	}

	Cursor::bewegen(0, 35);

}

//else if (!eingabeErlaubt(temp)) return;

void Spiel::spielzug()
{
	char input = SimpleInput::getSteuerung(CONFIGURATION::INTERVALL);

	if (input != CONFIGURATION::EINGABE_UNGUELTIG && eingabeErlaubt(input)) {

		// Baustein Select
		switch (input) {
			case CONFIGURATION::SPIELSTEIN_1_INDEX:
			{
				aktiverSpielstein = CONFIGURATION::SPIELSTEIN_1_INDEX;
				break;
			}
			case CONFIGURATION::SPIELSTEIN_2_INDEX:
			{
				aktiverSpielstein = CONFIGURATION::SPIELSTEIN_2_INDEX;
				break;
			}
			case CONFIGURATION::SPIELSTEIN_3_INDEX:
			{
				aktiverSpielstein = CONFIGURATION::SPIELSTEIN_3_INDEX;
				break;
			}
			case CONFIGURATION::SPIELSTEIN_4_INDEX:
			{
				aktiverSpielstein = CONFIGURATION::SPIELSTEIN_4_INDEX;
				break;
			}
			default:
			{
				aktuelleAufgabe.loesche();

				switch (input) {
					// Hoch/Runter/Links/Rechts
				case CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH);
					break;
				}
				case CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER);
					break;
				}
				case CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS);
					break;
				}
				case CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS);
					break;
				}
					// Rotationen
				case CONFIGURATION::SPIELSTEIN_ROTIEREN_RECHTS:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->rotation_rechts();
					break;
				}
				case CONFIGURATION::SPIELSTEIN_ROTIEREN_LINKS:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->rotation_links();
					break;
				}
					// Flip
				case CONFIGURATION::SPIELSTEIN_FLIP:
				{
					aktuelleAufgabe.getSpielstein(aktiverSpielstein)->flip();
					break;
				}
				default:
					break;

				}
				aktuelleAufgabe.zeichne();
				break;
			}
		}
	}	
	return;
}

// Noch nicht fertig
bool Spiel::eingabeErlaubt(char input)
{
	switch (input)
	{
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH:
		{
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH);
			if (aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung))	// aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung)
			{
				aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER);
				return false;
			}
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER);
			break;
		}
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER:
		{
			
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RUNTER);
			if (aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung))	// aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung)
			{
				aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH);
				return false;
			}
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_HOCH);
			break;
		}

		case CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS:
		{
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS);
			if (aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung)) // aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung)
			{
				aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS);
				return false;
			}
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS);
			break;
		}
		case CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS:
		{
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_LINKS);
			if (aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung)) // aktuelleAufgabe.getSpielstein(aktiverSpielstein)->ueberlapp(*umrandung)
			{
				aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS);
				return false;
			}
			aktuelleAufgabe.getSpielstein(aktiverSpielstein)->bewegen(CONFIGURATION::SPIELSTEIN_BEWEGEN_RECHTS);
			break;
		}
		break; 
	}
}