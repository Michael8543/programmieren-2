#include <iostream>

#include "Spielstein.h"
#include "Aufgabe.h"

Aufgabe::Aufgabe(Spielstein* stein1_ptr, Spielstein* stein2_ptr, Spielstein* stein3_ptr, Spielstein* loesung_ptr)
{
	steine.push_back(stein1_ptr);
	steine.push_back(stein2_ptr);
	steine.push_back(stein3_ptr);

	this->loesung_ptr = loesung_ptr;
}

Spielstein* Aufgabe::getSpielstein(unsigned int index)
{
	return steine.at(index); 
}

bool Aufgabe::geloest()
{
	bool temp = false;
	// �u�ere Schleife durchl�uft alle Positionen des Loesungssteins
	for (Position p : *loesung_ptr->getPositionen())
	{
		temp = false;
		// Innere Schleife Ruft alle Innerhalb() Funktionen aller Steine auf
		for (int j = 0; j < steine.size(); j++)
		{
			if (getSpielstein(j)->innerhalb(loesung_ptr->position + p))
				temp = true;
		}
		if (!temp) return false;
	}
	return true;
}


void Aufgabe::zeichne() {
	loesung_ptr->zeichne();
	for (Spielstein* spielstein : steine) {
		spielstein->zeichne();
	}
}


void Aufgabe::loesche() {
	loesung_ptr->loesche();
	for (Spielstein* spielstein : steine) {
		spielstein->loesche();
	}
}


