#ifndef SPIEL_H_
#define SPIEL_H_
#include <iostream>
#include "Aufgabe.h"

class Spiel
{
private:
	Aufgabe aktuelleAufgabe;
public:
	friend void testSpiel();
	
	int aktiverSpielstein = 0;
	Spielstein* umrandung = new Spielstein(SPIELSTEIN_UMRANDUNG);
	void init();

	
	void ausgabeLogo();
	void ausgabeRahmen();
	void ausgabeLegende();
	void ausgabeSteuerung();
	void spielSchleife();
	void spielzug();
	bool eingabeErlaubt(char input);
	void draw();

	Spiel(Aufgabe* aufgabe);
	//Spiel();
};

#endif /* SPIEL_H_ */
