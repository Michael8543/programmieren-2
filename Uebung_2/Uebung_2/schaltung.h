#pragma once

#include "baustein.h"
#include "schnittstelle.h"

#include <vector>

class Schaltung : public Baustein
{
protected:
	std::vector <Schnittstelle*> intern;
	std::vector <Baustein*> baustein;
};