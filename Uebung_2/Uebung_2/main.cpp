#include <iostream>
#include "xor2.h"
#include "oder2.h"
#include "und2.h"
#include "schnittstelle.h"
#include "baustein.h"
#include "halbaddierer.h"
#include "volladdierer.h"

void main()
{
	// Test Schnittstellen
	Schnittstelle e0;
	Schnittstelle e1;
	Schnittstelle e2;
	Schnittstelle a0;
	Schnittstelle a1;

	// Test Oder2
	Oder2* O2 = new Oder2(&e0, &e1, &a0);
	test2(O2);
	

	std::cout << "=======================\n" << std::endl;

	// Test Oder2
	Und2* U2 = new Und2(&e0, &e1, &a0);
	test2(U2);

	std::cout << "=======================\n" << std::endl;

	// Test Oder2
	Xor2* X2 = new Xor2(&e0, &e1, &a0);
	test2(X2);

	std::cout << "=======================\n" << std::endl;

	// Test HA
	Halbaddierer* HA = new Halbaddierer(&e0, &e1, &a0, &a1);
	test2(HA);
	
	std::cout << "=======================\n" << std::endl;

	// Test VA
	Volladdierer* VA = new Volladdierer(&e0, &e1, &e2, &a0, &a1);
	testVA2(VA);
	VA->~Volladdierer();

	std::cout << "=======================\n" << std::endl;
	return;
}

void test2(Baustein* b)
{
	for (int i = 0; i < 2; i++){
		for (int j = 0; j < 2; j++){
			b->eingang[0]->setPegel(i);
			b->eingang[1]->setPegel(j);
			b->update();
			b->print();
			std::cout << std::endl;
		}
	}
}

// Zus�tzliche for-schleife f�r den dritten Eingang des VA
void testVA2(Baustein* b)
{
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			for (int z = 0; z < 2; z++) {
				b->eingang[0]->setPegel(i);
				b->eingang[1]->setPegel(j);
				b->eingang[2]->setPegel(z);
				b->update();
				b->print();
				std::cout << std::endl;
			}
		}
	}
}

