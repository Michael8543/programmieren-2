#ifndef KENNZEICHEN_H_
#define KENNZEICHEN_H_


#include <string>
#include <iostream>
/* IHR CODE */

namespace kfz
{

	struct kennzeichen
	{
		std::string ort;
		std::string buchstaben;
		short zahl;
	};

	typedef struct kennzeichen kennzeichen;


	  /////////////////////////////////
	 //////////// Aufgabe 1 //////////
	/////////////////////////////////

	// Teilaufgabe a)
	//---------------
	std::string einlesenOrt()
	{
		char orta[10];

		while (true)
		{
			std::cout << "Bitte gehen sie eine Ortsabkuerzung ein (1-3): " << std::endl;
			std::cin >> orta;

			for (int i = 0; i < 5; i++)
			{
				if (orta[i] == '\0' && (i < 4))
				{
					std::cout << "Eingabe war erfolgreich" << std::endl;
					return orta;
				}
			}
			std::cout << "Die Ortsabkuerzung darf nur 1-3 Buchstaben umfassen!" << std::endl;
		}
	}


	// Teilaufgabe b)
	//---------------
	std::string einlesenBuchstaben()
	{
		while (true)
		{
			std::string input;

			std::cout << "Bitte geben sie 1-2 Buchstaben ein:" << std::endl;
			std::cin >> input;

			//
			if (input.length() > 0 && input.length() < 3)	// Prüfung ob eingabe zwischen 1-2 Zeichen besitzt
			{												// True = übergabe des String
				return input;								// False = neue Eingabe
			}
			std::cout << "Bitte erneut eingeben!" << std::endl;
		}
	}

	// Teilaufgabe c)
	//---------------
	short einlesenZahl()
	{
		unsigned short zahl = 0;

		while (true)
		{
			std::cout << "Bitte geben sie eine Zahl im bereich 1 - 9999 ein: " << std::endl;
			std::cin >> zahl;

			if ( 0 < zahl && zahl < 10000) //Liegt Zahl zwischen 1-9999?
			{
				return (short)zahl;
			}
		}
	}

	// Teilaufgabe d)
	//---------------
	kennzeichen* einlesen()
	{
		struct kennzeichen* newKennzeichen = new kennzeichen;
		std::cout << "Bitte waehlen sie sich ihr Wunschkennzeichen:" << std::endl;
		newKennzeichen->ort = einlesenOrt();
		newKennzeichen->buchstaben = einlesenBuchstaben();
		newKennzeichen->zahl = einlesenZahl();

		return (newKennzeichen);
	}
	// Teilaufgabe e)
	//---------------
	bool istSchnapszahl(const kennzeichen* pKennzeichen)
	{
        if (pKennzeichen->zahl < 100) 	return (pKennzeichen->zahl % 11 == 0);
        if (pKennzeichen->zahl < 1000) 	return (pKennzeichen->zahl % 111 == 0);
        return (pKennzeichen->zahl % 1111 == 0);

	}

	// Teilaufgabe f)
	//---------------

	bool istZehner(const kennzeichen* pKennzeichen)
	{
		if (pKennzeichen->zahl < 100 && (pKennzeichen->zahl % 10) == 0)
		{
			return true;
		}
		return false;
	}

	// Teilaufgabe g)
	//---------------

	bool istHunderter(const kennzeichen& rKennzeichen)
	{
		if (rKennzeichen.zahl < 1000 && (rKennzeichen.zahl % 100) == 0)
		{
			return true;
		}
		return false;
	}

	// Teilaufgabe h)
	//---------------

	bool istTausender(const kennzeichen* rKennzeichen)
	{
		if (rKennzeichen->zahl < 10000 && (rKennzeichen->zahl % 1000) == 0)
		{
			return true;
		}
		return false;
	}

	// Teilaufgabe i)
	//---------------

	void schildTest(kennzeichen* schild)
	{
		if (kfz::istZehner(schild))
		{
			std::cout << "Ein 10er Kennzeichen! Gute Wahl!" << std::endl;
		}

		if (kfz::istHunderter(*schild))
		{
			std::cout << "Ein 100er Kennzeichen! Gute Wahl!" << std::endl;
		}

		if (kfz::istTausender(schild))
		{
			std::cout << "Ein 1000er Kennzeichen! Gute Wahl!" << std::endl;
		}
	}

	// Teilaufgabe j)
	//---------------

	std::string ausgabe(const kennzeichen& rKennzeichen)
	{
		std::string temp;

		temp = rKennzeichen.ort + "-" + rKennzeichen.buchstaben + "-" + std::to_string(rKennzeichen.zahl);

		return temp;
	}
}
#endif /* KENNZEICHEN_H_ */
